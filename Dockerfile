FROM debian:11-slim

ADD [ "*.sh", "/usr/local/bin" ]

ENTRYPOINT [ "entrypoint.sh" ]
